cd cmd/shuffledns/

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip shuffledns

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf shuffledns /opt/ANDRAX/bin

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
